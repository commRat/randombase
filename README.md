# Overview
randombase is library for generating pseudorandom numbers and subsequent manipulation of that data.
You can generate random number in range of two numbers, generate list of random numbers in range from
A to B, list will be C long. Randombase can also generate random passwords, random strings, random
words or manipulate RNG components. You can read more in: Functions

# Installation
There will be 2 ways how to use randombase library:

## pip install randombase
Coming soon...

## Copy source code
Download randombase.py script from https://gitlab.com/commRat/randombase
Place that script into your workspace (same folder as your script). After
that you just simply add import to your script: import randombase

# Functions
Core of randombase library. Return random number in range: minimum_number - maximum_number

    randombase.random(minimum_number, maximum_number)

Creates a list of random number in range from minimum_number to
maximum_number. Size of the list is passed as 3rd param: list_length

    randombase.quantity_to_list(minimum_number, maximum_number, list_length)

Generates random float. First argument takes int & represents
how many symbols will be before dot. Second argument takes int &
represents how many symbols will be after dot

    randombase.float_gen(number_before_dot,number_after_dot)

Creates 18 symbols long, randomly generated password. Takes no arguments

    randombase.password()

User picks range where he wants to generate random number from A to B & from C to D
split(first_range_min,first_range_max,second_range_min,second_range_max) CD can be in
range of AB

    randombase.split(first_range_min,first_range_max,second_range_min,second_range_max)

Generates number, which is generated from 3 ranges A-B, c-d or E-F

    randombase.split_three(A,B,c,d,E,F)

Generates number, which is generated from 4 ranges A-B, c-d, E-F or g-h

    randombase.split_four(A,B,c,d,E,F,g,h)

Generates number, which is generated from 4 ranges A-B, c-d, E-F, g-h or I-J

    randombase.split_five(A,B,c,d,E,F,g,h,I,J)

Generates random string. Number of symbols will be choosed in range
from minimum_letters to maximum_letters

    randombase.string_gen(minimum_letters, maximum_letters)

In range from a to b, gives a probability percentage to 'section' from c to d
rest of percent goes to a from b range

    randombase.one_mod(a, b, percentage, c, d)

A to B is 'main range', P1 is percentual chance to pick number in range
of c-d, P2 is percentual chance to pick number in range e-f. Rest percentage,
goes to range A-B

    randombase.two_mod(A, B, P1, P2, c, d, e, f)

Generates random word. Takes no arguments

    randombase.word_gen()

Pick random element from list

    randombase.choice(_list)
