from Randombase import randombase
import matplotlib.pyplot as plt

# Define list with first value as zero, because we use last 
# element from each list, later on
one_list = [0]
two_list = [0]
three_list = [0]

run = 0

def last_not_zero_element(lst):
    all_elements = []
    for element in lst:
        if element != 0:
            all_elements.append(element)
    # If there is no max (list is empty), skip try & return 0
    try:
        return max(all_elements)
    except:
        return 0


while run < 200:
    x = randombase(1,3)

    if x == 1:
        new_item = last_not_zero_element(one_list) + 1
        one_list.append(new_item)
        two_list.append(two_list[-1])
        three_list.append(three_list[-1])

    elif x == 2:
        new_item = last_not_zero_element(two_list) + 1
        one_list.append(one_list[-1])
        two_list.append(new_item)
        three_list.append(three_list[-1])

    elif x == 3:
        new_item = last_not_zero_element(three_list) + 1
        one_list.append(one_list[-1])
        two_list.append(two_list[-1])
        three_list.append(new_item)
    
    run += 1



one_color = 'chocolate'
two_color = 'gold'
three_color = 'cornflowerblue'


# to list last element = max() element

# Creates an plot image
plt.plot(one_list, one_color, label='ONE')
plt.plot(two_list, two_color, label='TWO')
plt.plot(three_list, three_color, label='THREE')
plt.axis([1, 200, 0, 80])
plt.legend()
plt.show()

print(one_list)
print(two_list)
print(three_list)

