import randombase

testRandom = randombase.random(-20, 20)
print(testRandom)
# -3

testQuantity = randombase.quantityToList(0,1,10)
print(testQuantity)
# [1, 0, 1, 1, 0, 1, 0, 1, 0, 1]

testFloat = randombase.floatGen(3,4)
print(testFloat)
# 649.5473

testPassword = randombase.password()
print(testPassword)
# xZ!4AQ1nv04Z^*!Ah*

testSplit = randombase.split(0,9, 10,19)
print(testSplit)
# 13

testSplitThree = randombase.splitThree(1,3, 10,13, 20,23)
print(testSplitThree)
# 23

testSplitFour = randombase.splitFour(1,3,10,13,20,23,30,33)
print(testSplitFour)
# 2

testSplitFive = randombase.splitFive(1,3,10,13,20,23,30,33,40,43)
print(testSplitFive)
# 30

testStrGen = randombase.stringGen(5,12)
print(testStrGen)
# wuugsrlgoy

testOneMod = randombase.oneMod(1,100, 80, 90,100)
print(testOneMod)
# 98

testTwoMod= randombase.twoMod(1,1000, 40, 40, 1,5, 990,1000)
print(testTwoMod)
# 3

testWordGen = randombase.wordGen()
print(testWordGen)
# hotowo






