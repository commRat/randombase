#!/usr/bin/python

from setuptools import setup, find_packages
import pathlib


here = pathlib.Path(__file__).parent.resolve()
long_description = (here / 'README.md').read_text(encoding='utf-8')
__version__ = "1.0"

setup(
    name='randombase',
    version=__version__,
    description='Library which provides generating pseudo-random number and manipulation with them',
    long_description=long_description,
    url='https://gitlab.com/commRat/randombase',
    author='David Tomicek',
    author_email='tomicek.david@yahoo.com',
    keywords='generating numbers, random',
    packages=find_packages(),
)
