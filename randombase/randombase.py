from os import urandom as _urandom
import struct


def random(minimum_number, maximum_number):
    '''Core of randombase library. Return random number in range
    (minimum_number, maximum_number)'''

    def randombyte():
        random_byte = _urandom(4)
        test_unpack = struct.unpack('>HH', random_byte)
        unpacked_number = test_unpack[0]
        return unpacked_number

    def key_from_dict(item):
        for key, value in number_dict.items():
            if item == value:
                return key

        return 'Error. Non existing key.'

    number_dict = {}
    maximum_number += 1
    while minimum_number < maximum_number:
        number = randombyte()
        number_dict.update({minimum_number:number})
        minimum_number += 1
    max_value = max(number_dict.values())
    return key_from_dict(max_value)


def choose_obj(a, b):
    '''Pick randomly between two objects (accept str, int, obj, etc.)'''
    choice = random(1,2)
    return a if choice % 2 else b


def quantity_to_list(minimum_number, maximum_number, list_length):
    '''Creates a list of random number in range from minimum_number to
    maximum_number. Size of the list is passed as 3rd param: list_length'''
    generated_numbers = []
    i = 0
    while i < list_length:
        x = random(minimum_number,maximum_number)
        generated_numbers.append(x)
        i += 1
    return generated_numbers


def float_gen(number_before_dot,number_after_dot):
    '''Generates random float. First argument takes int & represents
    how many symbols will be before dot. Second argument takes int &
    represents how many symbols will be after dot.'''
    first_float = []
    second_float = []
    for _ in range(number_before_dot):
        x = random(0,9)
        y = str(x)
        first_float.append(y)
    half_float = ''.join([str(ele) for ele in first_float])
    for _ in range(number_after_dot):
        a = random(0,9)
        b = str(a)
        second_float.append(b)
    second_half_float = ''.join([str(ele) for ele in second_float])
    before_float = f'{half_float}.{second_half_float}'
    final_float = float(before_float)
    return final_float


def password():
    '''Creates 18 symbols long, randomly generated password. Takes no arguments'''
    chars = ['A', 'a', 'B', 'b', 'C', 'c', 'D', 'd', 'E', 'f', 'G', 'g', 'H', 'h',
         'I', 'i', 'J', 'j', 'K', 'k', 'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o',
         'P', 'p', 'Q', 'q', 'R', 'r', 'S', 's', 'T', 't', 'U', 'u', 'V', 'v',
         'W', 'w', 'X', 'x', 'Y', 'y', 'Z', 'z', '0', '1', '2', '3', '4', '5',
         '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
         '!', '#', '$', '%', '&', ')', '(', '*', '+', '-', '@', '<', '>', '^',
         '!', '#', '$', '%', '&', ')', '(', '*', '+', '-', '@', '<', '>', '^',]
    new_password = []
    password_lenght = 18  # Random between 10 & 18
    for _ in range(password_lenght):
        element = random(0,97)
        new_password.append(chars[element])
    new_password = ''.join([str(ele) for ele in new_password]) # Make a string from list elements
    return new_password


def split(first_range_min,first_range_max,second_range_min,second_range_max):
    '''User picks range where he wants to generate random number from A to B & from C to D
    split(first_range_min,first_range_max,second_range_min,second_range_max) CD can be in
    range of AB'''
    x = random(first_range_min, first_range_max)
    y = random(second_range_min, second_range_max)
    final_num = random(1,2)
    if final_num == 1:
        return x
    else:
        return y


def split_three(A,B,c,d,E,F):
    '''Generates number, which is generated from 3 ranges A-B, c-d or E-F'''
    x = random(A, B)
    y = random(c, d)
    z = random(E, F)
    final_num = random(1, 3)
    if final_num == 1:
        return x
    elif final_num == 2:
        return y
    else:
        return z


def split_four(A,B,c,d,E,F,g,h):
    '''Generates number, which is generated from 4 ranges A-B, c-d, E-F or g-h'''
    x = random(A, B)
    y = random(c, d)
    z = random(E, F)
    u = random(g, h)
    final_num = random(1, 4)
    if final_num == 1:
        return x
    elif final_num == 2:
        return y
    elif final_num == 3:
        return z
    else:
        return u


def split_five(A,B,c,d,E,F,g,h,I,J):
    '''Generates number, which is generated from 4 ranges A-B, c-d, E-F, g-h or I-J'''
    x = random(A, B)
    y = random(c, d)
    z = random(E, F)
    u = random(g, h)
    m = random(I, J)
    final_num = random(1, 5)
    if final_num == 1:
        return x
    elif final_num == 2:
        return y
    elif final_num == 3:
        return z
    elif final_num == 4:
        return u
    else:
        return m


def string_gen(minimum_letters, maximum_letters):
    '''Generates random string. Number of symbols will be choosed in range
    from minimum_letters to maximum_letters'''
    chars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
        'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
        'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',]
    new_string = []
    size = random(minimum_letters, maximum_letters)
    for _ in range(size):
        x = random(0,25)
        new = chars[x]
        new_string.append(new)
    final_string = ''.join([str(ele) for ele in new_string])
    return final_string


def one_mod(a, b, percentage, c, d):
    '''In range from a to b, gives a probability percentage to 'section' from c to d
    rest of percent goes to a from b range'''
    total_list = []
    first_list = []
    second_list = []
    rest = 100 - percentage
    if percentage <= 100:
        i = 0
        while i < percentage:
            x = random(c, d)
            first_list.append(x)
            i += 1
        i = 0
        while i < rest:
            y = random(a, b)
            second_list.append(y)
            i += 1
        total_list = first_list + second_list
        grab_from_list = random(1, 100)
        result = grab_from_list - 1
        return total_list[result]


def two_mod(A, B, P1, P2, c, d, e, f):
    '''A to B is 'main range', P1 is percentual chance to pick number in range
    of c-d, P2 is percentual chance to pick number in range e-f. Rest percentage,
    goes to range A-B'''
    total_list = []
    first_list = []
    second_list = []
    third_list = []
    rest = 100 - (P1+P2)
    if rest <= 100 and rest >= 0:
        i = 0
        while i < P1:
            x = random(c, d)
            first_list.append(x)
            i += 1
        i = 0
        while i < P2:
            y = random(e, f)
            second_list.append(y)
            i += 1
        i = 0
        while i < rest:
            z = random(A, B)
            third_list.append(z)
            i += 1
        total_list = first_list + second_list + third_list
        grab_from_list = random(1, 100)
        result = grab_from_list - 1
        return total_list[result]


def word_gen():
    '''Generates random word. Takes no arguments.'''
    chars = ['at', 'am', 'as', 'asu', 'aka', 'bo', 'byt',
        'be', 'ba', 'co', 'ca', 'cov', 'com', 'deu', 'do',
        'dam', 'es', 'eb', 'elo', 'fot', 'fab', 'gu', 'gy',
        'he', 'hot', 'ide', 'imo', 'jo', 'ja', 'kos', 'ky',
        'loo', 'ley', 'mit', 'mar', 'mos', 'no', 'neu', 'na'
        'nex', 'owo', 'oye', 'pho', 'phe', 'qo', 'xxa', 'ull',
        'uca', 'zon', 'tru', 'pam', 'nes', 'cen', 'yan', 'sun']
    new_string = []
    size = random(2, 3)
    for _ in range(size):
        x = random(0,53)
        new = chars[x]
        new_string.append(new)
    final_string = ''.join([str(ele) for ele in new_string])
    return final_string


def choice(_list):
    '''Pick random element from list'''
    list_len = len(_list) - 1
    return _list[random(0, list_len)]
